﻿using System.Collections;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CardsView : MonoBehaviour{

    [SerializeField] GridLayoutGroup m_gridPlayerCard;
    [SerializeField] GridLayoutGroup m_gridOpponentCards;
    [SerializeField] Transform m_currentDeckCardPosition;
    [SerializeField] Button m_btnHit;
    [SerializeField] Button m_btnHold;
    [SerializeField] Button m_btnEndTurn;
    [SerializeField] Text m_labelPlayerscore;
    [SerializeField] Text m_labelOpponentScore;
    [SerializeField] Text m_labelResult;
    [SerializeField] Text m_labelOpponentName;
    [SerializeField] Text m_labelPlayerName;
    [SerializeField] GameObject m_EndGameOverlay;
    [SerializeField] float m_dealSpeed = 2f; 
    

    public Card[] PlayerCards
    {
        get {
            var uiCards = m_gridPlayerCard.GetComponentsInChildren<UIDeckCard>();
            return uiCards != null ? uiCards.Select(x => x.Card).ToArray() : null;
        }
    }

    public Card[] OpponentCards
    {
        get
        {
            var uiCards = m_gridOpponentCards.GetComponentsInChildren<UIDeckCard>();
            return uiCards != null ? uiCards.Select(x => x.Card).ToArray() : null;
        }
    }

    public void InitView()
    {
        m_EndGameOverlay.gameObject.SetActive(false);
        var facedownCard = CardFactory.CreateCard(null);
        facedownCard.transform.SetParent(m_currentDeckCardPosition, false) ;
        facedownCard.transform.ResetLocal();        
    }

    public void SetPlayerScore(int score)
    {
        m_labelPlayerscore.text =  "Points:" +  (score >= 0 ? score.ToString() : "BUSTED");
    }

    public void SetOpponentScore(int score)
    {
        m_labelOpponentScore.text = "Points:" + (score >= 0 ? score.ToString() : "BUSTED");
    }


    public void AddCardToPlayer(Card newCard)
    {
        SetButtonsEnabled(false);
        AddCardInstant(newCard, m_gridPlayerCard.transform);
    }

    public void AddCardToOpponent(Card newCard)
    {
        SetButtonsEnabled(false);
        AddCardInstant(newCard, m_gridPlayerCard.transform);
    }

    public void AddCardToPlayer(Card newCard, Action onAnimFinish)
    {
        SetButtonsEnabled(false);
        AddCard(newCard, m_gridPlayerCard.transform, onAnimFinish);
    }

    public void AddCardToOpponent(Card newCard, Action onAnimFinish)
    {
        AddCard(newCard, m_gridOpponentCards.transform, onAnimFinish);
    }

    IEnumerator AnimateDealCard(Card newCard, Transform target, Action onAnimFinish)
    {
        var animationCard = CardFactory.CreateCard(null);
        animationCard.transform.SetParent(m_currentDeckCardPosition.transform);
        animationCard.transform.ResetLocal();
        Vector3 startPost = animationCard.transform.position;
        float t = 0;
        while (Vector3.Distance(animationCard.transform.position, target.transform.position) > 100f)
        {
            t += Time.deltaTime * m_dealSpeed;
            animationCard.transform.position = Vector3.Lerp(startPost, target.position, t);
            yield return null;
        }

        Destroy(animationCard.gameObject);

        var cardInstance = CardFactory.CreateCard(newCard);
        cardInstance.transform.SetParent(target, false);
        cardInstance.transform.ResetLocal();

        yield return null;

        onAnimFinish.Invoke();
    }

    void AddCard(Card newCard, Transform parent, Action onAnimFinish)
    {
        StartCoroutine(AnimateDealCard(newCard, parent, onAnimFinish));
    }

    void AddCardInstant(Card newCard, Transform parent)
    {
        var cardInstance = CardFactory.CreateCard(newCard);
        cardInstance.transform.SetParent(parent, false);
        cardInstance.transform.ResetLocal();        
    }

    public void EndPlayerTurn()
    {
        SetButtonsEnabled(false);
        SetOpponentTextColor(Color.white);
        SetPlayerTextColor(Color.white);
    }

    public void EndOpponentTurn()
    {
        SetButtonsEnabled(true);
        SetOpponentTextColor(Color.white);
        SetPlayerTextColor(Color.yellow);
    }

    public void SetButtonsEnabled(bool enabled)
    {
        m_btnEndTurn.interactable = enabled;
        m_btnHit.interactable = enabled;
        m_btnHold.interactable = enabled;
    }

    public void EndGame(int result)
    {
        SetButtonsEnabled(false);

        string message = "";
        if (result == 0) message = "DRAW";
        if (result == 1) message = "WIN";
        if (result == 2) message = "LOSE";

        m_labelResult.text = message;
        m_EndGameOverlay.gameObject.SetActive(true);
    }

    void ClearCards()
    {
        var uiCardsPlayer = m_gridPlayerCard.GetComponentsInChildren<UIDeckCard>();
        var uiCardsOpponent = m_gridOpponentCards.GetComponentsInChildren<UIDeckCard>();

        foreach (var card in uiCardsPlayer.Concat(uiCardsOpponent))
            Destroy(card.gameObject);

        SetPlayerScore(0);
        SetOpponentScore(0);
    }

    public void Reset()
    {
        SetOpponentTextColor(Color.white);
        SetPlayerTextColor(Color.yellow);

        ClearCards();
        SetPlayerScore(0);
        SetOpponentScore(0);
        SetButtonsEnabled(true);
        m_EndGameOverlay.gameObject.SetActive(false);
    }

    void SetPlayerTextColor(Color color)
    {
        m_labelPlayerName.color = color;
        m_labelPlayerscore.color = color;
    }

    void SetOpponentTextColor(Color color)
    {
        m_labelOpponentName.color = color;
        m_labelOpponentScore.color = color;
    }
}
