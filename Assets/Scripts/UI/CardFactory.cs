﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardFactory {

    const string BASE_PATH = "cards/";
    const string BASE_PREFAB_PATH = "UICardPrefab";

	public static UIDeckCard CreateCard(Card cardData)
    {
        string baseName = "card";
        string path = string.Empty;
        if (cardData == null)
        {
            path = BASE_PATH + baseName + "Back";
        }
        else
        {
            path = BASE_PATH + baseName + SuitToName(cardData.Suit) + FaceToName(cardData.Face);
        }

        var prefab = Resources.Load<UIDeckCard>(BASE_PREFAB_PATH);
        var newInstance = GameObject.Instantiate<UIDeckCard>(prefab);
        
        var sprite = Resources.Load<Sprite>(path);
        newInstance.SetSprite(sprite);
        newInstance.Card = cardData;
        return newInstance;
    }

    static string FaceToName(Face face)
    {
        if (face == Face.Ace) return "A";
        if (face < Face.Jack) return ((int)face).ToString();
        if (face == Face.Jack) return "J";
        if (face == Face.Queen) return "Q";
        if (face == Face.King) return "K";
        return string.Empty;
    }

    static string SuitToName(Suit suit)
    {
        switch(suit)
        {
            case Suit.Club: return "Clubs";
            case Suit.Diamond: return "Diamonds";
            case Suit.Heart: return "Hearts";
            case Suit.Spade: return "Spades";
        }

        return string.Empty;
    }
}
