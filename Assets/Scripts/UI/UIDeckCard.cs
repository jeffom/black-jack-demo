﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDeckCard : MonoBehaviour {

    [SerializeField]
    Image m_imgCard;

    public void SetSprite( Sprite spt )
    {
        m_imgCard.sprite = spt;
    }

    public Card Card { get; set; } //representation
}
