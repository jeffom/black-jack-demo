﻿using System;
using System.Linq;

public class BlackJackRulesChecker : IGameCardRules
{
    public int StartingHandCount
    {
        get
        {
            return 2;
        }
    }

    /// <summary>
    /// return 0 = tie
    /// return 1 = player 1 wins
    /// return 2 = player 2 wins
    /// </summary>
    /// <param name="player1"></param>
    /// <param name="player2"></param>
    /// <returns></returns>
    public int CompareHands(Card[] player1 , Card[] player2)
    {
        int player1Sum = GetScore(player1);
        int player2Sum = GetScore(player2);

        if (player1Sum == player2Sum)
            return 0;
        if (player1Sum > player2Sum)
            return 1;
        if (player1Sum < player2Sum)
            return 2;

        return -1;
    }

    public int GetScore(Card[] hand)
    {
        int sum = hand.Sum(x=>x.Value);          
        
        if (sum > 21)
        {
            bool hasAce = hand.Any(x => x.Face == Face.Ace);
            int aceCount = hand.Count(x => x.Face == Face.Ace);
            if (hasAce)
            {                
                for (int i = 0; i < aceCount; i++)
                {
                    if (sum > 21)
                        sum -= 10;
                }

                if (sum > 21)
                    return -1;
            }
            else return -1;
        }
        return sum;
    }

    public bool IsBusted(Card[] hand)
    {
        return GetScore(hand) < 0;
    }

    public bool HasDevaledAce(Card[] hand)
    {
        return hand.Any(x => x.Face == Face.Ace) && hand.Sum(x=>x.Value) > 21;
    }

    public int DevaledAceCount(Card[] hand)
    {
        int devaledAceCount = 0;
        int aceCount = hand.Count(x => x.Face == Face.Ace);
        int sum = hand.Sum(x => x.Value);
        for (int i = 0; i < aceCount; i++)
        {
            if (sum > 21)
            {
                devaledAceCount++;
                sum -= 10;
            }
        }

        return devaledAceCount;
    }
}
