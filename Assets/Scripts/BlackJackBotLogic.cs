﻿using System.Collections.Generic;
using System.Linq;

public class BlackJackBotLogic {

    BlackJackRulesChecker m_rulesCheck;

    public BlackJackBotLogic(IGameCardRules rulesChecker)
    {
        m_rulesCheck = rulesChecker as BlackJackRulesChecker;
    }

    public bool CanHitCard(Card[] hand, Card[] opponentHand)
    {
        bool hasAce = hand.Any(x => x.Face == Face.Ace);
        bool opponentHasAce = opponentHand.Any(x => x.Face == Face.Ace);
        
        int aceCount = hand.Count(x => x.Face == Face.Ace);
        int score = m_rulesCheck.GetScore(hand);
        int opponentScore = m_rulesCheck.GetScore(opponentHand);

        if (score > opponentScore)
        {            
            return false;
        }

        if (!hasAce || aceCount == m_rulesCheck.DevaledAceCount(hand))
        {            
            if (score < 12)
            {
                if (opponentHand.Any(x => x.Value == 4 || x.Value == 5 || x.Value == 6))
                    return true;
            }

            if (score < 13)
            {
                if (opponentHand.Any(x => x.Value == 2 || x.Value == 3))
                    return true;
            }

            if (score <= 17)
            {
                if (opponentHasAce || opponentHand.Any(x => x.Value > 7) || opponentScore > score)
                {
                    return true;
                }
            }

            if (opponentScore > score) return true; //in last case draw a caard

        }
        else if( hasAce && m_rulesCheck.DevaledAceCount(hand) == 0)
        {
            if (score >= 19) return false;
            if (score >= 18 && hand.Length >= 3)
            {
                if (opponentScore > score) return true;
                if (opponentHasAce) return true;
                if (opponentHand.Any(x => x.Value >= 9)) return true;
                return false;
            }
            return true;
        }

        return false;
    }
}
