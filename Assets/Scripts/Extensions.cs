﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions {

    public static void ResetLocal(this Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;        
    }
	
}
