﻿using System;
using System.Collections.Generic;

public class Deck
{
    Queue<Card> m_deckOfCards;
    public Queue<Card> CardsDeck { get { return m_deckOfCards; } }

    public Deck()
    {
        m_deckOfCards = new Queue<Card>();
        FillDeck();
    }

    void FillDeck()
    {
        m_deckOfCards.Clear();
        foreach(Suit s in Enum.GetValues(typeof(Suit)))
        {
            foreach (Face f in Enum.GetValues(typeof(Face)))
            {
                var card = new Card(s, f, GetFaceValue(f));
                m_deckOfCards.Enqueue(card);
            }
        }
    }

    public void ShuffleDeck()
    {
        List<Card> listofCards = new List<Card>();
        listofCards.AddRange(m_deckOfCards);
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < listofCards.Count; t++)
        {
            var tmp = listofCards[t];
            int r = UnityEngine.Random.Range(t, m_deckOfCards.Count);
            listofCards[t] = listofCards[r];
            listofCards[r] = tmp;
        }
        
        m_deckOfCards = new Queue<Card>(listofCards);
    }

    public static int GetFaceValue(Face face)
    {
        if (face == Face.Ace)
            return 11;
        if (face >= Face.Ten)
            return 10;
        else return (int)face;
    }
    
    public Card DealCard()
    {        
        return m_deckOfCards.Count > 0 ? m_deckOfCards.Dequeue() : null;
    }
}

