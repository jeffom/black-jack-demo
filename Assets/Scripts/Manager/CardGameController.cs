﻿using System.Collections;
using UnityEngine;
using System.Linq;


public class CardGameController : MonoBehaviour {

    const int PLAYER_INDEX = 0;
    const int OPPONENT_INDEX = 1;
    [SerializeField] CardsView m_gameView;
    BlackJackBotLogic m_botLogic;
    Deck m_gameDeck;
    IGameCardRules m_rules;
    bool[] m_handKept = new bool[2];
    bool m_hasAnimationEnded;

    public bool HasEnded
    {
        get
        {
            return !m_handKept.Any(x => !x);
        }
    }

    public int CurrentTurn { get; set; }

    void Awake()
    {        
        m_gameDeck = new Deck();        
        m_rules = new BlackJackRulesChecker();
        m_botLogic = new BlackJackBotLogic(m_rules);

        m_gameView.InitView();
    }

    void Start()
    {
        ResetGame();
    }
    
    public Card HitCard()
    {
        return m_gameDeck.DealCard();
    }

    public void HitPlayer()
    {
        m_hasAnimationEnded = false;
        m_gameView.AddCardToPlayer(HitCard(), ()=> {
            m_gameView.SetButtonsEnabled(true);
            m_hasAnimationEnded = true;
            CheckBustedHand();
            UpdateGameState();
        } );
    }

    public void HitOpponent()
    {
        m_hasAnimationEnded = false;
        m_gameView.AddCardToOpponent(HitCard(), () => {
            m_hasAnimationEnded = true;
            CheckBustedHand();
            UpdateGameState();
        } );
    }
    
    void CheckWinner()
    {
        var playerHand = m_gameView.PlayerCards;
        var opponentHand = m_gameView.OpponentCards;
    }
    
    public void EndPlayerTurn()
    {
        CurrentTurn = 1;
        m_gameView.EndPlayerTurn();
        UpdateGameState();
        StopAllCoroutines();
        StartCoroutine(WaitBot());
    }

    void EndOpponentTurn()
    {
        CurrentTurn = 0;
        m_gameView.EndOpponentTurn();
        UpdateGameState();
    }

    void EndGame(int result)
    {
        StopAllCoroutines();
        m_gameView.EndGame(result);
    }

    void CheckBustedHand()
    {        
        var playerHand = m_gameView.PlayerCards;
        var opponentHand = m_gameView.OpponentCards;
                                   
        if (m_rules.IsBusted(playerHand) && !m_rules.IsBusted(opponentHand))
            EndGame(2);
        else if (!m_rules.IsBusted(playerHand) && m_rules.IsBusted(opponentHand))
            EndGame(1);
        else if (m_rules.IsBusted(playerHand) && m_rules.IsBusted(opponentHand))
            EndGame(0);
    }

    void UpdateGameState()
    {
        var playerHand = m_gameView.PlayerCards;
        var opponentHand = m_gameView.OpponentCards;

        m_gameView.SetPlayerScore(m_rules.GetScore(playerHand));
        m_gameView.SetOpponentScore(m_rules.GetScore(opponentHand));

        if (HasEnded)
        {
            EndGame(m_rules.CompareHands(playerHand, opponentHand));
        }
    }

    public bool HasPlayerEnded(int playerIndex)
    {
        if (playerIndex < 0 || playerIndex >= m_handKept.Length) return false;
        return m_handKept[playerIndex];
    }
    
    public void KeepHand(int playerIndex)
    {
        if (playerIndex >= 0 && playerIndex < m_handKept.Length)
            m_handKept[playerIndex] = true;
    }

    IEnumerator WaitBot()
    {
        Debug.Log("WaitBot");        
        var botHand = m_gameView.OpponentCards;
        var playerHand = m_gameView.PlayerCards;
        while(m_botLogic.CanHitCard(botHand, playerHand) && !m_rules.IsBusted(botHand) && !HasEnded)
        {            
            HitOpponent();
            while (!m_hasAnimationEnded) yield return null;
            botHand = m_gameView.OpponentCards;            
        }

        while (!m_hasAnimationEnded) yield return null;

        if (HasPlayerEnded(PLAYER_INDEX))
        {            
            KeepHand(OPPONENT_INDEX);
            UpdateGameState();
        }
        else
            EndOpponentTurn();

        yield return null;
    }

    public void ResetGame()
    {
        StopAllCoroutines();
        StartCoroutine(ResetGameInternal());       
    }

    IEnumerator ResetGameInternal()
    {        
        m_gameDeck = new Deck();
        m_gameDeck.ShuffleDeck();

        CurrentTurn = 0;
        m_gameView.Reset();
        yield return null;

        for (int i = 0; i < m_handKept.Length; i++)
            m_handKept[i] = false;

        //deal cards
        for (int i = 0; i < m_rules.StartingHandCount; i++)
        {
            HitPlayer();
            HitOpponent();
        }

        m_gameView.SetButtonsEnabled(true);
    }
}
