﻿public class Card
{
    Suit m_suit;
    Face m_face;
    int m_value;

    public Suit Suit { get { return m_suit; } }
    public Face Face { get { return m_face; } }
    public int Value { get { return m_value; } }

    public Card(Suit suit, Face face, int value)
    {
        m_suit = suit;
        m_face = face;
        m_value = value;
    }

    public static bool operator == (Card x, Card y)
    {
        if (Equals(x, null)) return Equals(y,null);
        if (Equals(y, null)) return Equals(x, null);

        return x.Face == y.Face && x.Suit == y.Suit;
    }

    public static bool operator != (Card x, Card y)
    {
        return !(x == y);
    }

    public override bool Equals(object obj)
    {
        if (obj is Card)
            return this == (Card)obj;
        else
            return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}