﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameCardRules {
    int StartingHandCount { get; }      
    int CompareHands(Card[] player1, Card[] player2);
    int GetScore(Card[] hand);
    bool IsBusted(Card[] hand);
}
