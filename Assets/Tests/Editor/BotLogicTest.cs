﻿using NUnit.Framework;

public class BotLogicTest
{

    [Test]
    public void CheckHasNoAceRules()
    {

        var botHand = new Card[] {
            new Card(Suit.Spade, Face.Four, Deck.GetFaceValue(Face.Eight)),
            new Card(Suit.Spade, Face.Eight, Deck.GetFaceValue(Face.Eight))
        };

        var playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Six, Deck.GetFaceValue(Face.Six)),
        };

        var rulesChecker = new BlackJackRulesChecker();
        var botLogic = new BlackJackBotLogic(rulesChecker);

        //if the human player has an Ace or a card with 7 or more points, take a card if the AI
        //has less than 17 points
        Assert.True(botLogic.CanHitCard(botHand, playerHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Nine, Deck.GetFaceValue(Face.Nine)),
        };

        //Has less points dont take card
        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        //if the human player has a 4, 5, or 6, take a card if the AI has less than 12 points
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Three, Deck.GetFaceValue(Face.Two)),
        };

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Four, Deck.GetFaceValue(Face.Four)),
        };

        Assert.True(botLogic.CanHitCard(botHand, playerHand));

        botHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
        };

        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        //if the human player has a 2 or 3, take a card if the AI has less than 13 points
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
        };

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
        };

        Assert.True(botLogic.CanHitCard(botHand, playerHand));

        //otherwise don't take a card 
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Eight, Deck.GetFaceValue(Face.Eight)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
        };

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Eight, Deck.GetFaceValue(Face.Eight)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
        };

        Assert.False(botLogic.CanHitCard(botHand, playerHand));
    }

    [Test]
    public void CheckHasAceRules()
    {
        var botHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace))
        };

        var playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
        };

        var rulesChecker = new BlackJackRulesChecker();
        var botLogic = new BlackJackBotLogic(rulesChecker);
        Assert.True(botLogic.CanHitCard(botHand, playerHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Four, Deck.GetFaceValue(Face.Four)),
        };

        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        //don't take a card if the AI already has 19 or more points 
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Eight, Deck.GetFaceValue(Face.Eight)),
        };

        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        //don't take a card if the AI has 18 points and 3 or more cards, unless the opponent 
        //has an Ace or a card with 9 or more points
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Four, Deck.GetFaceValue(Face.Four)),
            new Card(Suit.Spade, Face.Three, Deck.GetFaceValue(Face.Three)),
        };

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
            new Card(Suit.Spade, Face.Nine, Deck.GetFaceValue(Face.Nine)),
        };

        Assert.True(botLogic.CanHitCard(botHand, playerHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
        };
        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Nine, Deck.GetFaceValue(Face.Nine)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
        };

        Assert.False(botLogic.CanHitCard(botHand, playerHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Six, Deck.GetFaceValue(Face.Six)),
            new Card(Suit.Spade, Face.Six, Deck.GetFaceValue(Face.Six)),
            new Card(Suit.Spade, Face.Seven, Deck.GetFaceValue(Face.Seven)),
        };

        Assert.True(botLogic.CanHitCard(botHand, playerHand));
        // Use the Assert class to test conditions.

        //otherwise take a card
        botHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Three, Deck.GetFaceValue(Face.Three)),
        };

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Nine, Deck.GetFaceValue(Face.Nine)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
        };
    }
}
