﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Linq;
using System;
using System.Collections;

public class DeckTest {

    [Test]
    public void TestDeckCreation()
    {
        Deck deck = new Deck();
        int count = 0;
        bool hasCard = false;
        foreach (Suit s in Enum.GetValues(typeof(Suit)))
        {
            foreach (Face f in Enum.GetValues(typeof(Face)))
            {
                hasCard = deck.CardsDeck.FirstOrDefault(x => x.Suit == s && x.Face == f) != null;
                if (!hasCard)
                    Assert.Fail();
                count++;
            }
        }

        Assert.AreEqual(count, 52);
    }

    [Test]
    public void TestDeal()
    {
        Deck deck = new Deck();
        for (int i = 0; i < 52; i++)
            deck.DealCard();

        Assert.AreEqual(0, deck.CardsDeck.Count);
    }

    [Test]
    public void TestShuffle()
    {
        UnityEngine.Random.InitState(1);
        Deck deck = new Deck(); //unshuffled
        

        int equalCount = 0;
        foreach (Suit s in Enum.GetValues(typeof(Suit)))
        {
            foreach (Face f in Enum.GetValues(typeof(Face)))
            {
                var drawCard = deck.DealCard();
                if (drawCard.Face == f && drawCard.Suit == s)
                    equalCount++;
            }
        }

        Assert.AreEqual(52, equalCount);
        deck = new Deck();
        deck.ShuffleDeck();
        equalCount = 0;
        foreach (Suit s in Enum.GetValues(typeof(Suit)))
        {
            foreach (Face f in Enum.GetValues(typeof(Face)))
            {
                var drawCard = deck.DealCard();
                if (drawCard.Face == f && drawCard.Suit == s)
                    equalCount++;
            }
        }

        Assert.AreNotEqual(52, equalCount);
    }

}
