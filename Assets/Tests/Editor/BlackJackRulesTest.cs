﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class BlackJackRulesTest
{

	[Test]
    //just adding a few deterministic cases to check if its correct for most unusual cases
	public void TestSumValidation() {
        // Use the Assert class to test conditions.
        var hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten))
        };

        var rulesCheck = new BlackJackRulesChecker();
        var handSum = rulesCheck.GetScore(hand);

        Assert.AreEqual(21, handSum);

        hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace))
        };

        handSum = rulesCheck.GetScore(hand);
        Assert.AreEqual(12, handSum);

        hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two))
        };

        handSum = rulesCheck.GetScore(hand);
        Assert.AreEqual(13, handSum);

        hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.King, Deck.GetFaceValue(Face.King)),
        };

        handSum = rulesCheck.GetScore(hand);
        Assert.AreEqual(-1, handSum);

        hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack)),
            new Card(Suit.Spade, Face.Queen, Deck.GetFaceValue(Face.Queen)),
            new Card(Suit.Spade, Face.King, Deck.GetFaceValue(Face.King)),
        };

        handSum = rulesCheck.GetScore(hand);
        Assert.AreEqual(-1, handSum);

        hand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ten, Deck.GetFaceValue(Face.Ten)),
            new Card(Suit.Spade, Face.Three, Deck.GetFaceValue(Face.Three)),
            new Card(Suit.Spade, Face.Five, Deck.GetFaceValue(Face.Five)),
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
        };

        handSum = rulesCheck.GetScore(hand);
        Assert.AreEqual(21, handSum);
    }    

    [Test]
    public void TestHandsCheck()
    {
        var rulesCheck = new BlackJackRulesChecker();

        var playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        var opponentHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two))
        };


        int result = rulesCheck.CompareHands(playerHand, opponentHand);
        Assert.AreEqual(1, result);
        result = rulesCheck.CompareHands(opponentHand, playerHand);
        Assert.AreEqual(2, result);

        opponentHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        result = rulesCheck.CompareHands(opponentHand, playerHand);
        Assert.AreEqual(0, result);
        result = rulesCheck.CompareHands(playerHand, opponentHand);
        Assert.AreEqual(0, result);

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack)),
            new Card(Suit.Spade, Face.King, Deck.GetFaceValue(Face.Jack)),
            new Card(Suit.Spade, Face.Queen, Deck.GetFaceValue(Face.Jack)),
        };

        Assert.IsTrue(rulesCheck.IsBusted(playerHand));
        result = rulesCheck.CompareHands(opponentHand, playerHand);
        Assert.AreEqual(1, result);
        result = rulesCheck.CompareHands(playerHand, opponentHand);
        Assert.AreEqual(2, result);
    }

    [Test]
    public void CheckWin()
    {
        var rulesCheck = new BlackJackRulesChecker();

        var playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        var opponentHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two))
        };

        Assert.AreEqual(1, rulesCheck.CompareHands(playerHand, opponentHand) );

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two)),
            new Card(Suit.Spade, Face.Two, Deck.GetFaceValue(Face.Two))
        };

        opponentHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        Assert.AreEqual(2, rulesCheck.CompareHands(playerHand, opponentHand));

        playerHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        opponentHand = new Card[] {
            new Card(Suit.Spade, Face.Ace, Deck.GetFaceValue(Face.Ace)),
            new Card(Suit.Spade, Face.Jack, Deck.GetFaceValue(Face.Jack))
        };

        Assert.AreEqual(0, rulesCheck.CompareHands(playerHand, opponentHand));
    }
}
